import Vue from 'vue'
import Router from 'vue-router'

// 导入响应的组件
import Login from "../views/Login"
import Main from "../views/Main"


export default new Router({
    routes:[    // 一组路由规则
            {
                // 登录页
                path: '/login',   // url
                name:'Login',   //组件名
                component: Login    // 注册进来
            },
            {
                // 首页
                path: '/main',
                name: 'Main',
                component: Main

            }

    ]
})
