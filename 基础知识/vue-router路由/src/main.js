// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

Vue.config.productionTip = false

// 导入ElementUI
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import VueRouter from 'vue-router'

// 安装路由
Vue.use(VueRouter);

// 安装ElementUI
Vue.use(ElementUI);

// 单页模式
new Vue({
  el: '#app',
  // 启用路由
  router,
  // 启用 ElementUI
    render: h=>h(App)
 
})
