# coding:utf-8
from flask import Flask,Response,jsonify
from flask_cors import CORS
app = Flask(__name__)
#自动在报文头部加入相应内容,解决跨域问题使用
CORS(app, resources=r'/*')

serverdata={
    "list": [{
            "id": 1,
            "name": "BaseService03",
            "cpu": "8核",
            "memory": "16G",
            "ip": "172.10.2.36",
            "state": "成功",
            "date": "2019-11-1"
        },
        {
            "id": 2,
            "name": "llp_web02",
            "cpu": "4核",
            "memory": "8G",
            "ip": "172.18.3.88",
            "state": "成功",
            "date": "2019-10-11"
        },
        {
            "id": 3,
            "name": "llp_web01",
            "cpu": "4核",
            "memory": "16G",
            "ip": "172.18.1.203",
            "state": "失败",
            "date": "2019-11-11"
        },
        {
            "id": 4,
            "name": "RedisSentinel01",
            "cpu": "2核",
            "memory": "4G",
            "ip": "172.18.0.204",
            "state": "成功",
            "date": "2019-10-20"
        }
    ],
    "pageTotal": 4
}

dbdata={
    "list": [{
            "id": 1,
            "name": "das",
            "type": "replicate",
            "engine": "mongodb",
            "version":"3.2",
            "date": "2019-11-1",
        },
        {
            "id": 2,
            "name": "rds21-app-ro",
            "type": "Readonly",
            "engine": "mysql",
            "version":"5.6",
            "date": "2019-10-11"

        },
        {
            "id": 3,
            "name": "rds30-uapi-query",
            "type": "Readonly",
            "engine": "mysql",
             "version":"5.6",
            "date": "2019-11-11"

        },
        {
            "id": 4,
            "name": "rds30-uapi-rw",
            "type": "Primary",
            "engine": "mysql",
             "version":"5.6",
            "date": "2019-10-20"
        }
    ],
    "pageTotal": 4
}


@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/api/serverlist')
def server():
    data={'a': 1, 'b': 2}
    return  jsonify(serverdata)

@app.route('/api/dblist')
def db():
    return  jsonify(dbdata)



if __name__ == '__main__':
    app.run(host="0.0.0.0")