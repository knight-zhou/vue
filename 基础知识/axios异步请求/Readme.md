### axios 官方文档
vue 官方文档推荐用axios 进行异步请求

vue2.0开始不再更新vue-resource进行异步请求

[axios官网](https://github.com/axios/axios)

```
<html>
<head>
<script src="http://jq.yeyese.top/axios/axios.min.js"></script>
</head>
<body>

<script>
    axios({
        method:'get',    // 请求的类型
        url:'http://127.0.0.1/jq/art/api.php',  // 请求的后端接口
        })
        .then(function (res) {     // 请求回调函数
         var res1 = res['data'];
            console.log(res1);
            console.log(typeof(res1));   // 浏览器是控制台输出是js对象，js中可以直接取值
            console.log(res1.name);
            console.log(res1['name']);  // 另一种方式输出
        });
</script>   

</body>
</html>
```

## axios 标准写法
```
axios({
	method:'get',  // 请求类型
	url:'http://127.0.0.1/jq/art/api.php',   // 请求url
	})
	.then(function (res) {     // .then 请求成功返回的数据
		console.log('it is:',res.data.name)
		vm.msg = res.data.name;
		console.log(vm.$data.msg)
		// console.log(vm);
		console.log(vm.$el);
	})
	.catch(function (error) {   // .catch 失败执行
		console.log(error);
	});
```
