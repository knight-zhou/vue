## vue-cli
[vue-cli official ](https://bitbucket.org/knight-zhou/html5)

## vue脚手架流程
```
   index.html->main.js->App.vue

1） 组件的基本组成部分
每个组件都包含 模板 template,逻辑处理页script 以及样式style 三个部分

2)脚手架中所有的都是网站文件是组件,其中App.vue 是根组件,其他组件需要和根组件进行关联才能显示，可以认为整个组件整合就是一个SPA(单页面)

3) 组件关联是通过组件注册进行的，有两种方式：
第一种方式 通过main.js 进行全局注册。
第二种方式  通过app.vue 进行局部注册(component)

4) 对于样式，如果不加 scoped 属性的话，会全局生效
```

### export default 是什么
export default 只能导出一个默认模块，这个模块可以匿名，引入的时候可以给这个模块取任意名字，例如 "obj"，且不需要用大括号括起来

### 取消路由后面的 # 的方法
在定义路由地址的时候，最后加上 mode: "history" 即可
