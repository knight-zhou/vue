import Vue from 'vue';
import App from './App.vue';
import router from './router';
import ElementUI from 'element-ui';
import VueI18n from 'vue-i18n';
import { messages } from './components/common/i18n';
import 'element-ui/lib/theme-chalk/index.css'; // 默认主题
import './assets/css/icon.css';
import './components/common/directives';
import 'babel-polyfill';

Vue.config.productionTip = false;
Vue.use(VueI18n);
Vue.use(ElementUI, {
    size: 'small'
});
const i18n = new VueI18n({
    locale: 'zh',
    messages
});

//使用钩子函数也就是拦截器进行权限控制，只有登陆后的用户才能访问设置的路由
router.beforeEach((to, from, next) => {
    document.title = `${to.meta.title} | Devops`;
    let role = sessionStorage.getItem('ms_role_id');   //获取角色id
    if (!role && to.path !== '/login') {
        next('/login');
    } else if (to.meta.permission) {
        // 如果是管理员权限则可进入，这里只是简单的模拟管理员权限而已
        // role === 'admin' ? next() : next('/403');
        role < 200 ? next() : next('/403');    // 是管理者可以打开,否则403
    } else {
        next();    // 富文本需要IE10以上才能打开，这里的判断就省略算了
    }
});

new Vue({
    router,
    i18n,
    render: h => h(App)
}).$mount('#app');
