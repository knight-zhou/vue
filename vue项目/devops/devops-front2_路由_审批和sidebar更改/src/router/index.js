import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);
//路由控制

export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/dashboard'
        },
        {
            path: '/',
            component: () => import('../components/common/Home.vue'),
            meta: { title: '自述文件' },
            children: [
                {
                    path: '/dashboard',
                    component: () => import('../components/page2/Dashboard.vue'),
                    meta: { title: '系统首页' }
                },
                {
                    path: '/icon',
                    component: () => import('../components/page2/Icon.vue'),
                    meta: { title: '自定义图标' }
                },
                {
                    path: '/table',
                    component: () => import('../components/page2/Table.vue'),
                    meta: { title: '基础图标模板' }
                },
                {
                    path : '/baolei',
                    component: () => import('../components/page2/Baolei.vue'),
                    meta: { title: '堡垒机外链' }

                },
                {
                    path: '/servertable',
                    component: () => import('../components/page2/ServerTable.vue'),
                    meta: { title: '服务器列表' }
                },
                {
                    path: '/dbtable',
                    component: () => import('../components/page2/DbTable.vue'),
                    meta: { title: '数据库列表' }
                },
                {
                    path: '/msg',
                    component: () => import('../components/page2/Msg.vue'),
                    meta: { title: '消息列表' }
                },
                {
                    path: '/form',
                    component: () => import('../components/page2/BaseForm.vue'),
                    meta: { title: '基本表单' }
                },
                {
                    // 权限页面
                    path: '/permission',
                    component: () => import('../components/page2/Permission.vue'),
                    meta: { title: '权限测试', permission: true }
                },
                {
                    path: '/404',
                    component: () => import('../components/page2/404.vue'),
                    meta: { title: '404' }
                },
                {
                    path: '/403',
                    component: () => import('../components/page2/403.vue'),
                    meta: { title: '403' }
                },
                {
                    path: '/test',
                    component: () => import('../components/page2/Test.vue'),
                    meta: { title: '自定义测试页面'}
                },
                {
                    path : '/umanager',
                    component: () => import('../components/page2/Umanager.vue'),
                    meta: { title: '用户管理',permission: true}
                },
                {
                    path : '/super',
                    component: () => import('../components/page2/Super.vue'),
                    meta: { title: '超级用户管理'}
                }
            ]
        },
        // 作业平台
        {
            path: '/task',
            redirect: '/dashboard'
        },
        {
            path: '/task',
            component: () => import('../components/common/Home.vue'),
            meta: { title: 'task页面'},
            children: [
                {
                    path : '/task/tasklist',
                    component: () => import('../components/task/Tasklist.vue'),
                    meta: { title: '任务列表执行页'}
                },
                {
                    path : '/task/script',
                    component: () => import('../components/task/Script.vue'),
                    meta: { title: '脚本管理'}
                },
                {
                    path : '/task/playbook',
                    component: () => import('../components/task/Playbook.vue'),
                    meta: { title: '剧本管理'}
                }  
            ]
        },
        //  子路由设置
        {
            path: '/sys',
            redirect: '/dashboard'
        },
        {
            path: '/sys',
            component: () => import('../components/common/Home.vue'),
            meta: { title: 'sys页面'},
            children: [
                {
                    path : '/sys/super',
                    component: () => import('../components/page2/Super.vue'),
                    meta: { title: '超级用户管理'}
                } 
            ]
        },
        {
            path: '/login',
            component: () => import('../components/page2/Login.vue'),    //  打开首页跳到登陆页面需要认证登录
            meta: { title: '登录' }
        },
        {
            path: '*',
            redirect: '/404'
        }
    ]
});
