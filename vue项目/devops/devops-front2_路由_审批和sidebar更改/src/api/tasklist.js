import request from '../utils/request';
/*
所有数据源的途径
*/

// 没有后端就直接读取json文件模拟数据

export const fetchData = query => {
    return request({
       url: '/api/task/tasklist',    // http://127.0.0.1:8080/dbtable.json
        method: 'get',
        params: query     // 参数为 query,前端可以直接query.xx取值
    });

};

// 返回数据库表的数据
