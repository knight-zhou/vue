
// dev env配置
// https://segmentfault.com/a/1190000010043013
module.exports = {
    // publicPath: '/',
    // assetsDir: 'static',
    // productionSourceMap: false,

    // 设置代理
    devServer: {
        proxy: {
            '/api':{
                target:'http://127.0.0.1:5000/api',  // 后端接口地址
                changeOrigin:true,
                pathRewrite:{
                    '/api':''
                }
            }
        }
    },

    //警告 webpack 的性能提示
    configureWebpack : {
    performance: {
        hints:'warning',
        //入口起点的最大体积 整数类型（以字节为单位）
        maxEntrypointSize: 50000000,
        //生成文件的最大体积 整数类型（以字节为单位 300k）
        maxAssetSize: 30000000,
        //只给出 js 文件的性能提示
        assetFilter: function(assetFilename) {
            return assetFilename.endsWith('.js');
        }
    }
}

}