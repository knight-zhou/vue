# coding:utf-8
from flask import Flask,Response,jsonify,request
from flask_cors import CORS
import os
import platform
from werkzeug.utils import secure_filename
from vars import dbdata,serverdata,tasklist_data,scriptlist_data,playbooklist_data
app = Flask(__name__)

################定义上传属性
UPLOAD_FOLDER = 'upload/ops'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# 创建上传文件夹路径
if platform.system() == "Windows":
  slash = '\\'
else:
  platform.system()=="Linux"
  slash = '/'

if not os.path.exists(UPLOAD_FOLDER):
    os.makedirs(UPLOAD_FOLDER)
else:
    pass
## 指定长传的文件名
ALLOW_EXTENSIONS = set(['txt','zip','sh','py'])

def allowed_file(filename):
    return '.' in filename and \
            filename.rsplit('.', 1)[1] in ALLOW_EXTENSIONS

#####################################
#自动在报文头部加入相应内容,解决跨域问题使用
CORS(app, resources=r'/*')

"""
role_id 为101,就是管理员，其他用户的角色都是 role_id为1001开头
uer_id 从101开始
    http://127.0.0.1:5000/api/task/tasklist
"""

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/api/serverlist')
def server():
    data={'a': 1, 'b': 2}
    return  jsonify(serverdata)

## 模拟数据库的删除操作
# @app.route('/api/deldb/<int:id>')
@app.route('/api/deldb')
def delserver():
    id = request.args.get('id')
    id = int(id)   # 转字符串
    del(dbdata["list"][id-1])
    print(dbdata)
    return dbdata


@app.route('/api/dblist')
def db():
    engine = request.args.get("engine")
    name = request.args.get("name")
    print("前端传过来的参数为"+engine+"----"+name)   # 可以通过orm 查库了
    return  jsonify(dbdata)

## 数据库更新接口
@app.route('/api/dbupdate')
def dbupdate():
    name = request.args.get("name")
    type = request.args.get("type")
    print("update 参数为:" +name+"---"+type)
    return "update db success---"

## 作业列表接口
@app.route('/api/task/tasklist')
def tasklist():
    return jsonify(tasklist_data)

## 脚本管理列表
@app.route('/api/task/scriptlist')
def scriptlist():
    return jsonify(scriptlist_data)

### 新建脚本接口
@app.route('/api/addscript')
def addscript():
    # 接收参数
    name = request.args.get("name")
    type = request.args.get("type")
    remark = request.args.get("remark")
    print("新增脚本页参数为:" + name + "---" + type+"---"+remark+"---")
    return "add script success"

### 文件上传接口
@app.route('/api/upload_file',methods=['GET','POST'])
def  upload_file():
    if request.method =='POST':
        #获取post过来的文件名称，从name=file参数中获取
        # file = request.files['file']
        file = request.files['file']
        print(file)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)   # secure_filename方法会去掉文件名中的中文
            file.save(os.path.join(app.config['UPLOAD_FOLDER'],filename))
            base_path = os.getcwd()
            file_path = base_path + slash + app.config['UPLOAD_FOLDER'] + slash + filename  # 最后的文件路径
            print(file_path)
            return jsonify({"success": 0, "msg": "upload sucess"})
        else:
             return jsonify({"error": 1001, "msg": "upload failed"})
    return "please upload file"


####
## 剧本管理列表
@app.route('/api/task/playbooktlist')
def playbooktlist():
    return jsonify(playbooklist_data)



## 登录接口
"""
http://127.0.0.1:5000/api/login?username=knight&password=knight    登录成功
"""
@app.route('/api/login')
def login():
    username=request.args.get("username")
    password=request.args.get("password")
    if username == "knight" and password == "knight":
        info={"msg":"login success","userinfo":{"username":"knight","role_name":"普通用户","role_id":1001}}
        return jsonify(info)
    elif username == "admin"  and password =="admin":
        info = {"msg": "login success", "userinfo": {"username": "admin", "role_name": "管理员", "role_id": 101}}
        return jsonify(info)
    else:
        return jsonify({'msg': None})


if __name__ == '__main__':
    app.run(host="0.0.0.0",debug=True)