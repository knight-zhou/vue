# coding:utf-8
dbdata={
    "list": [{
            "id": 1,
            "name": "das",
            "type": "replicate",
            "engine": "mongodb",
            "version":"3.2",
            "date": "2019-11-1",
        },
        {
            "id": 2,
            "name": "rds21-app-ro",
            "type": "Readonly",
            "engine": "mysql",
            "version":"5.6",
            "date": "2019-10-11"

        },
        {
            "id": 3,
            "name": "rds30-uapi-query",
            "type": "Readonly",
            "engine": "mysql",
             "version":"5.6",
            "date": "2019-11-11"

        },
        {
            "id": 4,
            "name": "rds30-uapi-rw",
            "type": "Primary",
            "engine": "mysql",
             "version":"5.6",
            "date": "2019-10-20"
        }
    ],
    "pageTotal": 4
}


###
serverdata={
    "list": [{
            "id": 1,
            "name": "BaseService03",
            "cpu": "8核",
            "memory": "16G",
            "ip": "172.10.2.36",
            "state": "成功",
            "date": "2019-11-1"
        },
        {
            "id": 2,
            "name": "llp_web02",
            "cpu": "4核",
            "memory": "8G",
            "ip": "172.18.3.88",
            "state": "成功",
            "date": "2019-10-11"
        },
        {
            "id": 3,
            "name": "llp_web01",
            "cpu": "4核",
            "memory": "16G",
            "ip": "172.18.1.203",
            "state": "失败",
            "date": "2019-11-11"
        },
        {
            "id": 4,
            "name": "RedisSentinel01",
            "cpu": "2核",
            "memory": "4G",
            "ip": "172.18.0.204",
            "state": "成功",
            "date": "2019-10-20"
        }
    ],
    "pageTotal": 4
}

## 作业列表,一个作业最多只能绑定一个脚本和一个剧本
tasklist_data={
    "list": [{
            "id": 1,
            "name": "清除dns缓存",
            "step": 1,
            "creator": "knight",
            "bind_script_id": 301,
            "bind_playbook_id": 601,
            "status": 0,
            "date": "2019-11-1"
        },
        {
            "id": 2,
            "name": "打印所有主机名",
            "step": 2,
            "creator": "admin",
            "bind_plok_spt_name": "打印所有主机名",
            "status": 0,
            "date": "2019-11-2"
        },
		{
		    "id": 3,
            "name": "Clean cache",
            "step": 4,
            "creator": "admin",
            "bind_plok_spt_name": "Clean cache",
            "status": 1,
            "date": "2019-11-3"
		}
    ],
    "pageTotal": 3
}
## 脚本管理列表列表,脚本列表从301开始计数，剧本列表从601开始计数
scriptlist_data={
    "list": [{
            "id": 301,
            "name": "清除dns缓存",
            "remark":"清除dns缓存 ---描叙",
            "type": "shell",
            "creator": "knight",
            "filepath": "/home/data/task/dns.sh",
            "status": 0,
            "date": "2019-11-1"
        },
        {
            "id": 302,
            "name": "打印所有主机名",
            "remark":"打印所有主机名 ---描叙",
            "type": "python",
            "creator": "admin",
            "filepath": "/home/data/task/hostname.sh",
            "status": 0,
            "date": "2019-11-2"
        },
       {
             "id": 303,
            "name": "Clean cache",
            "remark":"Clean cache ---描叙",
            "type": "yml",
            "creator": "admin",
            "filepath": "/home/data/task/clean_cache.yaml",
            "status": 1,
            "date": "2019-11-3"
     }
    ],
    "pageTotal": 3
}

### 剧本管理列表
playbooklist_data={
    "list": [{
            "id": 601,
            "name": "安装mysql",
            "remark":"安装mysql ---描叙",
            "creator": "knight",
            "filepath": "/home/data/task/mysql.yml",
            "status": 0,
            "date": "2019-12-1"
        },
        {
            "id": 602,
            "name": "更新oms系统",
            "remark":"更新oms系统---描叙",
            "creator": "admin",
            "filepath": "/home/data/task/update.sh",
            "status": 0,
            "date": "2019-12-2"
        },
       {
             "id": 603,
            "name": "安装redis",
            "remark":"安装redis---描叙",
            "creator": "admin",
            "filepath": "/home/data/task/redis.yml",
            "status": 1,
            "date": "2019-12-3"
     }
    ],
    "pageTotal": 3
}