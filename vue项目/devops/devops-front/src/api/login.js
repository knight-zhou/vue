import request from '../utils/request';
/*
所有数据源的途径
*/

// 没有后端就直接读取json文件模拟数据

export const fetchData = query => {
    return request({
       url: '/api/login',    // 登录接口
        method: 'get',        // 为了方便调试暂时使用get方法
        params: query     // 参数为 query,前端可以直接query.xx取值
    });

};

// 返回数据库表的数据
