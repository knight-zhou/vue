import Vue from 'vue';
import Router from 'vue-router';


Vue.use(Router);


export default new Router({
	routes: [
		{
			path: '/',
			component: () => import('../components/common/Home.vue')
		},
		{
			path: '/table',
			component: () => import('../components/page/BaseTable.vue')
		}
	]
})