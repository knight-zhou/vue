import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

// 嵌套路由
export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/home'   // 必须加重定向不然子路由无法执行 
        },
        {
            path: '/',
            component: () => import('../components/common/Home.vue'),
            meta: { title: '自述文件' },
            children: [
                {
                    path: '/home',
                    component: () => import('../components/page/Dashboard.vue'),
                    meta: { title: '系统大盘页' }
                },
                {
                    path: '/dbtable',
                    component: () => import('../components/page/DbTable.vue'),
                    meta: { title: '数据库列表' }
                },
                {
                    path: '/404',
                    component: () => import('../components/page/404.vue'),
                    meta: { title: '404' }
                },
                {
                    path: '/test',
                    component: () => import('../components/page/Test.vue'),
                    meta: { title: '自定义测试页面'}
                }
            ]
        },
        {
            path: '/test2',
            component: () => import('../components/page/Test2.vue'),
            meta: { title: '平级的测试页面'}
        }
  
    
    ]
});
