//
module.exports = {
    // 设置代理
    devServer: {
        proxy: {
            '/api':{
                target:'http://127.0.0.1:5000/api',  // 后端接口地址
                changeOrigin:true,
                pathRewrite:{
                    '/api':''
                }
            }
        }
    }
}