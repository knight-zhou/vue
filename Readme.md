## 学习链接
[学习链接-菜鸟教程](http://www.runoob.com/vue2/vue-install.html)
##
[中文官方翻译文档](https://cn.vuejs.org/)

## 公司前端同事推荐ivew
[ivew ](http://v1.iviewui.com/)

### npm慢的话用淘宝镜像
cnpm代替npm

### 使用vue的好处
对于自己来说，可以去请求第三方的api或者开源的api 比如翻译接口，地图接口等，这样看感觉像后端语言一样

## 最基本核心功能
* v-bind : 动态绑定指令,默认情况下标签自带属性的值是固定的，在为了能够动态的给这些属性添加值
缩写用`:`代替即可

* v-if: 条件语句

* v-for: 循环语句

* v-on: 事件监听器,监听事件然后触发函数,缩写用`@`符号代替

* v-model: 在表单<input> (text,checkbox,radio),<select>及<textarea> 元素上创建双向数据绑定,它会忽略表单元素的初始值而将vue实例的数据作为数据来源
```
jQuery 也可以实现数据双向绑定但是就比较麻烦,代码比较多,可以百度搜(jQuery 双向数据绑定), jq 使用arttemplate实现
```

## 高级功能
### 模板语法
+ 插值
	+ `created` 钩子: 用来在一个实例被创建之后执行代码
	+ v-once: 执行一次性的插值,当数据改变时候就不会改变
	+ 双大括号会将数据解释为普通文本，而非HTML代码,要输出html，需要使用 `v-html`

### 计算属性和侦听器
对于复杂逻辑应该使用 计算属性,任何计算属性都可以用方法实现.他们的区别在于计算属性是有缓存的性能更高
```
var vm = new Vue({
  el: '#demo',
  data: {
    firstName: 'Foo',
    lastName: 'Bar'
  },
  computed: {
    fullName: function () {
      return this.firstName + ' ' + this.lastName
    }
  }
})
```
在 Vue 中，computed 的属性可以被视为是 data 一样，可以读取和设值，因此在 computed 中可以分成 getter(读取) 和 setter（设值），一般情况下是没有 setter 的，computed 预设只有 getter ，也就是只能读取，不能改变设值

##### 全局还是局部css
添加参数scoped，作用域为当前组件，否者为全局样式
```
<style scoped>
  .item{
     color: green;
}
</style>
```

#### scope 理解
scope的row 就是打出表格的这一行数据是对象,
index就是对象的index,下标从0开始
```
<el-table-column width="120">
    <template slot-scope="scope">                                    
    <el-button type="danger" @click="handleDetail(scope.$index,scope.row)">详情</el-button>
</el-table-column>

<script>
methods: {

            //已通过的详情展示页
            // handleDetail(index) {
            handleDetail(index,row) {
                // console.log(row)             // 显示详情
                console.log(row.title);
              
            }
			
			}
</script>	
	
```

######  根据后台api传过来的status 是0或者1 来判断前端显示
```
                <el-table-column prop="date" label="创建时间" ></el-table-column>
                <el-table-column prop="status" label="状态" >
                    <template slot-scope="scope">
                        <span style="color:green">{{ scope.row.status | filStatus}}</span>
                    </template>
                </el-table-column>
				
				
		<script>
			filters:{
				filStatus(v){
					if(v === 0){
						return '未通过'
					} else {
						return '已通过'
					}
				}
			},
		</script>	
```

##### 做项目笔记
(1) 在写vue的时候匹配的路由最好写全路径,那样就不会出现404情况
```
          <el-menu-item-group>
            <el-menu-item index="/servertable" >服务器</el-menu-item>
            <el-menu-item index="/dbtable" >数据库</el-menu-item>
          </el-menu-item-group>
        </el-submenu>  
```
前台拿到数据之后还要进行过滤使用 filter 函数即可
```
    methods: {
        // 获取 get的数据,只有已经审批过的才会在列表页显示
        getData() {
            fetchData(this.query).then(res => {
                console.log(res); 
                // this.tableData = res.list.filter( a => a.status === 1) ;   // 简写方法,然后省略的写法
                this.tableData = res.list.filter(function(xx){
                    return xx.status === 1;

                }) ;
                this.pageTotal = res.pageTotal || 50;
            });
        },
```


##### 打包
```shell
npm run build   
```


#### css深度选择器
由于层级太多了，一个写麻烦可以直接从父级到想要的css元素上
```
 <div class="xx">
 
 </div>

/* 深度选择器 */
.xx >>> .el-upload--text{
    width: 100px  !important;
    height: 40px;
}

```

#### 跳转到外链
```
      <el-menu-item >
            <span @click="OnClickBl()">堡垒机</span>
        </el-menu-item> 
		
		
<script>
    methods: {
      OnClickBl(){
        console.log("你点击了堡垒机");
        window.open("https://www.baidu.com");        // 跳转到外链

    }
</script>
```
