import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/Home'
import About from "../components/About";
import shopfooter from "../components/common/shopfooter";

Vue.use(Router)

export default new Router({
  // mode: 'history',
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      // 注意了，这个是相对路径，下面的子路由也要相对路径，如果home加了斜杠是绝对路径，下面的子路由也要绝对路径
      path: '/home',
      component: Home,
    },
    {
      path: '/about',
      name: 'About',
      component: About
    }
  ]
})

