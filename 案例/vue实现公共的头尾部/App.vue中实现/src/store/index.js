import Vue from 'vue'
import Vuex from 'vuex'


// 挂载Vuex
Vue.use(Vuex)

// 创建Vue对象

const store = new Vuex.Store({
    state:{
        name: 'hello_xxoo',
        age: 20
    },
    getters:{
        nameInfo(state){
            return "姓名"+ state.name
        },
        fullinfo(state,getters){
            return getters.nameInfo+"年龄:"+state.age
        }
    }

})
export default store
