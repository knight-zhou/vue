import Vue from 'vue'
import Router from 'vue-router'
import Login from '../components/Login'
import Home from '../components/Home'
import NavMenu from '../components/common/NavMenu'
import About from "../components/About";
import User from "../components/user/User";

Vue.use(Router)

export default new Router({
  // mode: 'history',
  routes: [
    {
      path: '/home',
      name: 'Home',
      component: Home,
      // redirect: '/index'
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    /*新增user路径，配置了动态的id*/
    {
      path: "/user/:id",
      component: User
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      children: [
        {
          path: '/nav',
          name: 'NavMenu',
          component: NavMenu
        }
      ]
    },
    {
      path: '/w-nav',
      name: 'NavMenu',
      component: NavMenu
    }
  ]
})

