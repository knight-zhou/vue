#### 静态路由

静态路由文件夹，都是严格匹配的，只有router-link 中的to属性和 js 中一条路由route中 path 一模一样，才能显示相应的组件component.。



#### 动态路由

在动态路由中，怎么获取到动态部分？ 因为在组件中是可以显示不同部分的，就是上面提到的“你的名字”。其实，当整个vue-router 注入到根实例后，在组件的内部，

可以通过 `this.$route` 来获取到 router 实例。它有一个params 属性，就是来获得这个动态部分的。它是一个对象，属性名，就是路径中定义的动态部分 id, 属性值就是router-link中to 属性中的动态部分，如123。

使用vuex时，组件中想要获取到state 中的状态，是用computed 属性，在这里也是一样，在组件中，定义一个computed 属性 **dynamicSegment**

```html
<template>
    <div>
        <h1> 我是user组件</h1>
        <div>
            我是user组件，动态部分是 {{ dynamicSegment  }}
        </div>
    </div>

</template>

<script>
    export default {
        computed: {
            dynamicSegment(){
                return this.$route.params.id
            }
        }
    }
</script>

<style scoped>

</style>
```

#### 嵌套路由

　嵌套路由，主要是由我们的页面结构所决定的。当我们进入到home页面的时候，它下面还有分类，如手机系列，平板系列，电脑系列。当我们点击各个分类的时候，它还是需要路由到各个部分，如点击手机，它肯定到对应到手机的部分。

　　在路由的设计上，首先进入到 home ,然后才能进入到phone, tablet, computer. Phone, tablet, compute 就相当于进入到了home的子元素。所以vue 提供了childrens 属性，它也是一组路由,相当于我们所写的routes。

　　首先，在home页面上定义三个router-link 标签用于导航，然后再定义一个router-view标签，用于渲染对应的组件。

**tips:** 

嵌套路由一定要在home的页面也加渲染器 `<router-view></router-view>`，因为要渲染子路由里的东西。

当然你不想要嵌套路由，直接第一层路由即可，然后路径用绝对路径就行了。



#### 命名路由

命名路由，很简单，因为根据名字就可以知道，这个路由有一个名字，那就直接给这个路由加一个name 属性，就可以了。 给user 路由加一个name 属性：

```
{
        path: "/user/:id",
        name: "user",
        component: user
}
```

命名路由的使用, 在router-link 中to 属性就可以使用对象了, 

```
<router-link to="/user/123">User123</router-link> // 和下面等价 
 <router-link :to="{ name: 'user', params: { userId: 123 }}">User</router-link>   // 当使用对象作为路由的时候，to前面要加一个冒号,表示绑定
```

