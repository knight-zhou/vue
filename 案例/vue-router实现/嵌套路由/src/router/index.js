import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/Home'
import NavMenu from '../components/common/NavMenu'
import About from "../components/About";
import User from "../components/user/User";
import Phone from "../components/home/Phone";
import Tablet from "../components/home/Tablet";
import Computer from "../components/home/Computer";

Vue.use(Router)

export default new Router({
  // mode: 'history',
  routes: [
    {
      // 注意了，这个是相对路径，下面的子路由也要相对路径，如果home加了斜杠是绝对路径，下面的子路由也要绝对路径
      path: '/home',
      component: Home,
      children: [
        {
          path: "phone",
          component: Phone
        },
        {
          path: "tablet",
          component: Tablet
        },
        {
          path: "computer",
          component: Computer
        },
          // 当进入到home时，下面的组件显示
        {
          path: "",
          component: Phone
        }
      ]
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    /*新增user路径，配置了动态的id*/
    {
      path: "/user/:id",
      component: User
    },
    {
      path: '/w-nav',
      name: 'NavMenu',
      component: NavMenu
    },
    {
      path: '/xx/phone',
      component: Phone
    }
  ]
})

