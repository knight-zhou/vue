from flask import Flask, jsonify,request
from flask_cors import *
import json


app = Flask(__name__)
CORS(app, supports_credentials=True)  # 设置跨域

@app.route("/get")
def hi():
    return "this is get request"

@cross_origin
@app.route("/api/login",methods=['POST'])
def getUserInfo():
    # print(request.form)
    # print(request.headers)
    # res = request.get_data()
    # res_data = str(res, encoding = "utf-8")
    # print(res_data)
    data = json.loads(request.get_data(as_text=True))
    print(data)
    print(type(data))
    print("----=====")
    print(request.headers)
    return "....."


if __name__ == "__main__":
    app.run(host="127.0.0.1", debug=True, port=5000)