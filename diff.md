## vue.js 和 node.js 区别
```
vue.js 是前端框架，node.js 既可以做前端也可以做后端
```

## vue.js 和 jquery的对比
```
大家普遍认为，对于新手web程序员来说，jQuery被认为是一个好的入门点，甚至于很多程序员都是先学jQuery，再学原生Javascript。
为什么呢？首先是因为jQuery非常流行，但更重要的是，有经验的程序员都认为jQuery十分简单，推己及人，对于新手来说当然也应该是非常简单的，然而这并不正确。
jQuery简洁，但不简单。


jQuery绝对可以帮你处理掉很多老浏览器的麻烦事（译者注：比如各种兼容性问题），但对于简化DOM API操作和封装Javascript变化而言，它就帮不上什么忙了。
当然，对于敲 $(‘#id’).click(function(event) {..}); 这样代码的时候，肯定比原生的Javascript要简洁。但一些知识你是必须要掌握的，比如DOM选择器，事件处理，回调函数等等，不然你可写不出上述的代码。
jQuery能让你更简单地写代码，前提是你懂得DOM API和Javascript的相关知识，但这对新手来说并不容易


Vue.js
Vue.js是Javascript世界的新宠，我几乎可以说它是最好学的。简约之道，深入其中。
我坚信，当一名新手使用Vue.js编写完一个简单的应用后，会比他使用jQuery（编写同一应用）更好地理解代码是如何工作的。
那么，让我们一起动手，分别用Vue.js和jQuery编写一个相同的简单应用，看看那个更好理解。这个应用会计算点击按钮的次数，并显示在显示出来。
```
## vue.js 特点
```
简洁： HTML 模板 + JSON 数据，再创建一个 Vue 实例，就这么简单。
数据驱动： 自动追踪依赖的模板表达式和计算属性。
组件化： 用解耦、可复用的组件来构造界面。
轻量： ~24kb min+gzip，无依赖。
快速： 精确有效的异步批量 DOM 更新。
模块友好： 通过 NPM 或 Bower 安装，无缝融入你的工作流。
```
[vue-resource已经废弃的链接](https://cn.vuejs.org/v2/cookbook/adding-instance-properties.html)
