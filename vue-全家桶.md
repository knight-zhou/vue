### vue 全家桶
* 1) 项目构建工具: vue-cli
* 2) 路由: vue-router(http://router.vuejs.org)
* 3) 状态管理 vuex(http://vuex.vuejs.org)
* 4) http请求工具 v-resource(https://github.com/pagekit/vue-resource)  不建议使用，已经废弃，建议使用ajax等

## vue-resource
```
如果我们不需要使用jQuery的DOM选择器，就没有必要引入jQuery。vue-resource是Vue.js的一款插件，它可以通过XMLHttpRequest或JSONP发起请求并处理响应。也就是说，$.ajax能做的事情，vue-resource插件一样也能做到，而且vue-resource的API更为简洁。
vue-resource插件具有以下特点：

1) 体积小
vue-resource非常小巧，在压缩以后只有大约12KB，服务端启用gzip压缩后只有4.5KB大小，这远比jQuery的体积要小得多。
支持主流的浏览器

2)和Vue.js一样，vue-resource除了不支持IE 9以下的浏览器，其他主流的浏览器都支持。
支持Promise API和URI Templates

3)Promise是ES6的特性，Promise的中文含义为“先知”，Promise对象用于异步计算。
URI Templates表示URI模板，有些类似于ASP.NET MVC的路由模板。

4)支持拦截器
拦截器是全局的，拦截器可以在请求发送前和发送请求后做一些处理。
拦截器在一些场景下会非常有用，比如请求发送前在headers中设置access_token，或者在请求失败时，提供共通的处理方式

5)
vue2.0之后，就不再对vue-resource更新，而是推荐使用axios。基于 Promise 的 HTTP 请求客户端
```

### vue中写js
可以尝试在methods里面调用JS，然后@click=""调用时就会直接执行JS里面的函数了

## vue 安装
```
vue-cli create hello 选择默认的
如果要安装vue-router等
cnpm install vue-route -S -D

```