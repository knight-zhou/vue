## webpack
webpack 是一个现代 JavaScript 应用程序的静态模块打包器 (module bundler) 。当 webpack 处理应用程序时，它会递归地构建一个依赖关系图 (dependency graph) ，其中包含应用程序需要的每个模块，webpack 主要有四个核心概念:

* 入口 (entry)
* 输出 (output)
* loader
* 插件 (plugins)

## 基础语法
```
let 声明的变量只在 let 命令所在的代码块内有效
const 声明一个只读的常量，一旦声明，常量的值就不能改变
```


### 学习文档
[es6学习文档](http://www.runoob.com/w3cnote/es6-class.html)

## export与export default 区别
* export与export default均可用于导出常量、函数、文件、模块等
* 你可以在其它文件或模块中通过import+(常量 | 函数 | 文件 | 模块)名的方式，将其导入，以便能够对其进行使用
* 在一个文件或模块中，export、import可以有多个，export default仅有一个
* 通过export方式导出，在导入时要加{ }，export default则不需要

这样来说其实很多时候export与export default可以实现同样的目的，只是用法有些区别。注意第四条，通过export方式导出，在导入时要加{ }，export default则不需要。使用export default命令，为模块指定默认输出，这样就不需要知道所要加载模块的变量名。

